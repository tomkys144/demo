for running dev-server use
```
npm run serve
```
or 
```
yarn serve
```

for production build use
```
npm run build
```
or
```
yarn build
```

and deploy from *dist* folder.

In **VUEX** stores, I didn't feel the need to split into modules and separate getters, mutations, etc. since this state tracker is really simple.

Result page is hosted on [demo.tkysela.cz](https://demo.tkysela.cz)
