import { createStore } from "vuex";

export default createStore({
    state () {
        return {
            suggestions: []
        }
    },
    mutations: {
        addSuggestion (state, item) {
            state.suggestions.unshift(item);
        }
    },
    getters: {
        getSuggestion: (state) => (employer) => {
            return state.suggestions.find(suggestion => suggestion.employer === employer);
        },
        suggestionExist: (state) => (employer) => {
            return !!state.suggestions.find(suggestion => suggestion.employer === employer);
        }
    }
});