import { createApp } from 'vue'
import store  from '/store'
import App from './App.vue'
import axios from "axios";
import VueAxios from "vue-axios";

const app = createApp(App)

app.use(store)

app.use(VueAxios, axios)

app.mount('#app')

